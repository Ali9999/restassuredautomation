package com.sample.test;
import static io.restassured.RestAssured.*;

import org.testng.annotations.Test;

public class TestDelete {

	@Test
	public void testDelete() {
		
		baseURI = "https://reqres.in";
		
		when().
		delete("/api/users/2").
		then().statusCode(204).
		log().all();
	}
	
}
