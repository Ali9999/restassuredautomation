package com.sample.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TestExamples {

	@Test // Test NG Annotation to indicate the test method
	public void testGet() {

		/*
		 * getting the response from the GET url and 
		 * storing it in the response variable
		 */
		Response response = RestAssured.get("https://reqres.in/api/users?page=2");

		// to get the status code of the url
		System.out.println("===This is status code===");
		System.out.println(response.getStatusCode() + "\n");

		// to get the response time of the url
		System.out.println("===This is response time===");
		System.out.println(response.getTime() + "\n");

		// to get the complete response body
		System.out.println("===This is body of response===");
		System.out.println(response.getBody().asString() + "\n");

		// to get the status line
		System.out.println("===This is status line===");
		System.out.println(response.getStatusLine() + "\n");

		// to log the content type header
		System.out.println("===This is content type===");
		System.out.println(response.getHeader("content-type") + "\n");

		// to do any assertion
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
	}
	

}
