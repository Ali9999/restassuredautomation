package com.sample.test;

//Static import of the restassured library gives direct access to its methods
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import org.testng.annotations.Test;

import io.restassured.response.Response;

public class TestExamples2 {

	@Test
	public void testGet() {

		/*
		 * Static import of the restassured library gives direct access to its methods
		 * compare with previous class example -- testGet() method
		 */
		Response response = get("https://reqres.in/api/users?page=2");
		System.out.println("===This is status code===");
		System.out.println(response.getStatusCode() + "\n");
	}
	
	//********Check this one in this class************
	@Test
	public void test2() {
		/*
		 * storing the base uri and end point separately.
		 * Endpoint is given in the get session.
		 * and checked for the satus code
		 */
		
		baseURI = "https://reqres.in";
		given().
		get("/api/users?page=2").
		then().
		statusCode(200).
		
		//To check the value at given position is same-- assertion
		body("data[1].id",equalTo(8))
		
		//we can also log the response
		.log().all();
	}
	
	
}
