package com.sample.test;

//Static import of the restassured library gives direct access to its methods
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

public class TestGetPost {

	@Test
	public void testGet() {
		baseURI = "https://reqres.in";
		given().get("/api/users?page=2").then().statusCode(200).

		// This time checking for the string value present in the response body
				body("data[4].first_name", equalTo("George")).

				// This time checking for the list strings
				body("data.first_name", hasItems("George", "Rachel"));

	}

	@Test
	public void testPost() {
		//To post the data we are using map collection in java which later has to be converted to json object
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("name", "Padma");
		map.put("job","Tester");
		System.out.println(map);
		
		//Converting the map data to json data
		JSONObject req = new JSONObject(map);
		System.out.println(req);
		
		//We can directly use the json object to create the json data for the post request
		JSONObject request = new JSONObject();
		request.put("name", "Padma");
		request.put("job", "QualityEngineer");
		
		
		baseURI = "https://reqres.in";
		
		given().
		header("Content-Type","application/json").
		contentType(ContentType.JSON).
		body(request.toJSONString()).
		when().post("/api/users").
		then().statusCode(201).
		log().all();
		
		
		
	}
}
