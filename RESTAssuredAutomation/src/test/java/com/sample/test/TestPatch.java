package com.sample.test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

public class TestPatch {

	@Test
	public void testPatch() {
		
		
		JSONObject request = new JSONObject();
		request.put("name", "Padma");
		request.put("job", "QualityEngineer");
		
		
		baseURI = "https://reqres.in";
		
		given().
		header("Content-Type","application/json").
		contentType(ContentType.JSON).
		body(request.toJSONString()).
		when().patch("/api/users/2").
		then().statusCode(200).
		log().all();

	}
	
}
