package com.sample.test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

public class TestPut {

	@Test
	public void testPut() {
		
		JSONObject request = new JSONObject();
		request.put("name", "Padma");
		request.put("job", "QualityEngineer");
		
		
		baseURI = "https://reqres.in";
		
		given().
		header("Content-Type","application/json").
		contentType(ContentType.JSON).
		body(request.toJSONString()).
		when().put("/api/users/2").
		then().statusCode(200).
		log().all();

	}
	
}
